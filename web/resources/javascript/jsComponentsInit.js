function adjustFlipCardHeight(id)
{
    var captionHeight = 0;

    $(id + " div.back").children().each(function()
    {
        captionHeight = captionHeight + $(this).height();
    });

    if($(id).height() < captionHeight)
    {
        $(id).height(captionHeight + 15);
    }
}

function setFlipCardHeight(id)
{
    var cardHeight = 0;

    $(id + " div.front").children().each(function()
    {
        cardHeight = cardHeight + $(this).height();
    });

    $(id).height(cardHeight);
}

function adjustFlipSlides()
{
    $(".flip-slide").each(function()
    {
        var cardHeight = 0;

        $(this).find("div.front").children().each(function()
        {
            cardHeight = cardHeight + $(this).height();
        });

        $(this).height(cardHeight);
    });
}

function initializeFlipCards()
{
    for(var i = 0;i < flipCardElements.length;i++)
    {
        setFlipCardHeight(flipCardElements[i]);

        $(flipCardElements[i]).flip();

        $(flipCardElements[i]).on('flip:done',{eName: flipCardElements[i]}, function(event)
        {
            var flipCard = $(this).data("flip-model");

            if(flipCard.isFlipped)
            {
                adjustFlipCardHeight(event.data.eName);
            }
            else
            {
                setFlipCardHeight(event.data.eName);
            }
        });
    }

    $(".flip-slide .back").each(function(){ $(this).show(); });
}

$(document).ready(function()
{
    window.setTimeout(function(){
        initializeFlipCards();
    }, 200);
});

$(window).resize(function()
{
    for(var i = 0;i < flipCardElements.length;i++)
    {
        var flipCard = $(flipCardElements[i]).data("flip-model");

        if(flipCard.isFlipped)
        {
            adjustFlipCardHeight(flipCardElements[i]);
        }
        else
        {
            setFlipCardHeight(flipCardElements[i]);
        }
    }

});
