function adjustSlideShowStyling()
{
    $(".hero").each(function()
    {
        $(this).removeClass("xs-hero-slider");
        $(this).removeClass("sm-hero-slider");
        $(this).removeClass("md-hero-slider");
        $(this).removeClass("lg-hero-slider");

        if($(this).width() <= 640)
        {
            $(this).find(".split-pane").each(function()
            {
                $(this).addClass("sm-split-pane");

                $(this).children('div').each(function()
                {
                    if($(this).hasClass("ui-g-6"))
                    {
                        $(this).removeClass("ui-g-6");
                        $(this).addClass("ui-g-12");
                    }
                });


            });
        }
        else
        {
            $(this).find(".split-pane").each(function()
            {
                $(this).removeClass("sm-split-pane");

                $(this).children('div').each(function()
                {
                    if($(this).hasClass("ui-g-12"))
                    {
                        $(this).removeClass("ui-g-12");
                        $(this).addClass("ui-g-6");
                    }
                });
            });
        }

        if($(this).find(".split-pane").length > 0)
        {
            var bxViewPort = $(this).parent();

            var tallestSlide = 0;
            
            $(this).find(".slide").each(function()
            {
                if(tallestSlide < $(this).height())
                {
                    tallestSlide = $(this).height();
                }
            });

            bxViewPort.height(tallestSlide);
        }

        if(window.innerWidth > 640)
        {
            if ($(this).width() < 351)
            {
                $(this).addClass("sm-hero-slider");
                $(this).addClass("xs-hero-slider");
            }

            if ($(this).width() > 350 && $(this).width() < 770)
            {
                $(this).addClass("sm-hero-slider");
            }
        }

        if($(this).width() > 769 && $(this).width() < 901)
        {
            $(this).addClass("md-hero-slider");
        }

        if($(this).width() > 900 && $(this).width() < 1280)
        {
            $(this).addClass("lg-hero-slider");
        }
    });
}

$(window).on("load", function()
{
     adjustSlideShowStyling();
});

$(window).resize(function(){
    adjustSlideShowStyling();
});