/**
 * Created by user on 10/21/15.
 */

$( document ).ready(function()
{
    $(".show-urls-link").click(function()
    {
        var urlsContainer = $($(this).parent().parent().find(".urls-debug"));

        if(urlsContainer.is(':visible'))
        {
            urlsContainer.slideUp();
        }
        else
        {
            urlsContainer.slideDown();
        }

        return false;
    });

    removeMenuThemeIcons();

    $("#layout-menu li.active-menu-parent").each(function(){
        $(this).removeClass("active-menu-parent");
    });

    $("#layout-menu a.active-menu").each(function(){
        $(this).removeClass("active-menu");
    });

    $("button.logout-button").hover(function () {
        $(this).addClass('ui-state-hover');
    },function() {$(this).removeClass('ui-state-hover')});

    $("div.content-bodies img").each(function(){
        $(this).css('height','auto');
    });

    $("div.content-bodies-2 img").each(function(){
        $(this).css('height','auto');
    });

    $("div.preload").each(function(){ $(this).show(); });

    $(".assignment-edit-menu span.ui-menubutton").each(function()
    {
        $(this).find("span.ui-icon").each(function(){$(this).removeClass("ui-icon")});

        $(this).find("span.ui-button-text").remove();
    });

    adjustVideoOverlayText();

    if(!isMobileBrowser())
    {
        $(".top-media-display div.vid-background-fixed-container").css("height", window.innerHeight + "px");
    }

    $(".top-media-display div.vid-fit-container").css("height", window.innerHeight + "px");
});

function addRegionDebugFunctionality()
{
    $( ".region-list" ).sortable(
    {
        connectWith: ".region-list",
        handle: ".portlet-handlebar",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all",
        tolerance: "pointer",
        scrollSensitivity: 150,
        scrollSpeed: (isMobileBrowser())?10:20,
        stop: function( event, ui )
        {
            var assignmentIndex = $(ui.item).index();
            var assignmentId = $(ui.item).attr('id').substr($(ui.item).attr('id').lastIndexOf('-') + 1);
            var regionId = $(ui.item).parent().attr('region');

            $.get("/update-region?assignmentId=" + assignmentId + "&regionId=" + regionId + "&index=" + assignmentIndex);
        }
    });

    addMoveToClickHandler();

    $("div.layout-debug div.region-list").each(function()
    {
        var regionHeader = $(this).prev();

        regionHeader.append("<a class='add-assignment-link' style='float: right' href='#' onclick='showPiaQuickDialog(" + $(this).attr('region') + "," + $(this).children("div [id*='assignment-']").length + ");return false;'><span class='icon-plus-circle'></span></a>");
    });
}

window.addEventListener('orientationchange', function()
{
    window.setTimeout(function(){
        adjustVideoOverlayText();
    }, 1500);
});

$(window).resize(function()
{
    if(!isMobileBrowser())
    {
        adjustVideoOverlayText();
    }
});

function addMoveToClickHandler()
{
    $(".move-assignment-link").unbind('click');

    $(".move-assignment-link").click(function()
    {
        var moveContainer = $($(this).parent().parent().find(".assignment-move-to"));

        if(moveContainer.is(':visible'))
        {
            moveContainer.slideUp();
        }
        else
        {
            moveContainer.slideDown();
        }

        return false;
    });
}

function moveToPageRegion(assignmentId, oldRegionId, regionId)
{
    if(!isNaN(assignmentId) && !isNaN(oldRegionId) && !isNaN(regionId))
    {
        var pageAssignment = $(".assignment-" + assignmentId);
        pageAssignment.remove();
        
        $("div[region='" + oldRegionId + "']").sortable('refresh');

        var region = $("div[region='" + regionId + "']");
        region.append(pageAssignment);
        region.sortable("refresh");
        
        var assignmentIndex = region.sortable("toArray").length - 1;

        $.get("/update-region?assignmentId=" + assignmentId + "&regionId=" + regionId + "&index=" + assignmentIndex);
    }

    addMoveToClickHandler();
}

function removeMenuThemeIcons()
{
    $(".iberis-menu span.ui-menuitem-icon").each(function(){$(this).removeClass('ui-icon')});

    $(".iberis-menu span.ui-submenu-icon").each(function(){$(this).removeClass('ui-icon')});

    $(".ui-menubar .prime-sub-group span.ui-icon").each(function(){
        if($(this).hasClass('ui-icon-triangle-1-s'))
        {
            $(this).removeClass('ui-icon-triangle-1-s');
            $(this).addClass('ui-icon-triangle-1-e');
        }
    });

    $("div.iberis-menu img").each(function(){
        $(this).css('height','auto');
    });

    $("ul.iberis-menu img").each(function(){
        $(this).css('height','auto');
    });

    $("div.iberis-mobile-menu img").each(function(){
        $(this).css('height','auto');
    });

    $('.preload').show();
}

function deleteAssignment(id)
{
    $.get("/delete-assignment?assignmentId=" + id, null, function(){window.location.reload()});
}

function showPiaQuickDialog(regionId, index)
{
    $('#quick-pia-iframe').attr('src', '');
    $('#quick-pia-iframe').attr('src', '/cms/quick-page-item-assignment-region/' + regionId + '/' + index);
    PF('quick-pia-dialog').show();
}

function showPiaEditQuickDialog(id)
{
    $('#quick-pia-iframe').attr('src', '');
    $('#quick-pia-iframe').attr('src', '/cms/quick-pia-edit/' + id);
    PF('quick-pia-dialog').show();
}

function showPresentationQuickDialog(id)
{
    $('#quick-presentation-iframe').attr('src', '');
    $('#quick-presentation-iframe').attr('src', '/cms/quick-presentation-edit/' + id);
    PF('quick-presentation-dialog').show();
}

function showMenuQuickDialog(id)
{
    $('#quick-menu-iframe').attr('src', '');
    $('#quick-menu-iframe').attr('src', '/cms/quick-menu-edit/' + id);
    PF('quick-menu-dialog').show();
}

function showVideoQuickDialog(id)
{
    $('#quick-video-iframe').attr('src', '');
    $('#quick-video-iframe').attr('src', '/cms/quick-video-edit/' + id);
    PF('quick-video-dialog').show();
}

function showImageQuickDialog(id)
{
    $('#quick-image-iframe').attr('src', '');
    $('#quick-image-iframe').attr('src', '/cms/quick-image-edit/' + id);
    PF('quick-image-dialog').show();
}

function showContentQuickDialog(id)
{
    $('#quick-content-iframe').attr('src', '');
    $('#quick-content-iframe').attr('src', '/cms/quick-content-edit/' + id);
    PF('quick-content-dialog').show();
}

function showGalleryQuickDialog(id)
{
    $('#quick-gallery-iframe').attr('src', '');
    $('#quick-gallery-iframe').attr('src', '/cms/quick-gallery-edit/' + id);
    PF('quick-gallery-dialog').show();
}

function submitDynamicForm(fieldsContainerName, formId)
{
    var fieldContainer = $("div[id$=" + fieldsContainerName + "]");

    var fieldData = new Array();

    fieldData = addToFieldDataArray(fieldContainer, "input[id$=_TEXT]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "input[id$=_DATE_input]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "input[id$=_MONEY_input]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "textarea[id$=_TEXTAREA]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "textarea[id$=_EDITOR]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "textarea[id$=_CODE]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "select[id$=_SELECTION_input]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "input[id$=_NUMBER_input]", fieldData);
    fieldData = addToFieldDataArray(fieldContainer, "select[id$=_BOOLEAN_input]", fieldData);

    dFormSubmit([{name: 'fieldData', value: JSON.stringify(fieldData)}, {name:'formId', value: formId}, {name: 'fieldsContainerName', value: fieldsContainerName}]);
}

function addToFieldDataArray(fieldContainer, fieldName, fieldData)
{
    var fields = fieldContainer.find(fieldName);

    fields.each(function()
    {
        var field = new Object();
        field.name = $(this).attr('id').substr($(this).attr('id').lastIndexOf(':') + 1);
        field.value = $(this).val();
        field.displayValue = $(this).parent().prev().find("label").html();

        fieldData.push(field);
    });

    return fieldData;
}

function highlightErrorFields(fieldsContainerName)
{
    var fieldContainer = $("div[id$=" + fieldsContainerName + "]");

    var requiredFields =  fieldContainer.find("*[aria-required=true]");

    requiredFields.each(function()
    {
        var labelField = fieldContainer.find("label[for$=" + $(this).attr('id').substr($(this).attr('id').lastIndexOf(':') + 1) +"]");

        if($(this).val() == '')
        {
            if(!$(this).hasClass('ui-state-error'))
            {
                $(this).addClass('ui-state-error');
                labelField.addClass('ui-state-error');
            }
        }
        else
        {
            if($(this).hasClass('ui-state-error'))
            {
                $(this).removeClass('ui-state-error');
                labelField.removeClass('ui-state-error');
            }
        }
    });
}

/*function initializeFieldLists()
{
    $( ".field-list" ).sortable({
        connectWith: ".field-list",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all Container25 Responsive",
        stop: function( event, ui ) {
            var assignmentIndex = $(ui.item).index();
            var fieldId = $(ui.item).attr('id').substr($(ui.item).attr('id').lastIndexOf('-') + 1);
            var sectionId = $(ui.item).parent().attr('section');

            updateSection([{name:'fieldId', value:fieldId}, {name:'sectionId', value:sectionId}, {name: 'index', value: assignmentIndex}]);
        }
    });
} */

function scrollToElement(elementId)
{
    /*
        {
        behavior: "smooth", // or "auto" or "instant"
        block: "start" // or "end"
    }
     */
    $(elementId)[0].scrollIntoView(true);

    window.scrollBy(0, -110);
}


function adjustVideoOverlayText()
{
    $(".video-overlay-caption").each(function(){

        if($(this).parent().width() >= 650)
        {
            $(this).css("font-size","1.5em");
        }
        else if($(this).parent().width() >= 450)
        {
            $(this).css("font-size","1.3em");
        }

        if($(this).parent().width() <= 250)
        {
            $(this).css("font-size",".8em");
        }

    });
}

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

