## Iberis CMS
<img src="http://jimwebguru.gitlab.io/iberis/iberis-color1a.png"/>

### Demo videos:

Desktop:
https://youtu.be/dDru7ZcolWI

Mobile:
https://youtu.be/4G0WkrlaCgQ

/**********************************************************/

Quick Note:

The Iberis CMS was created by me, jimwebguru. I am the creator and only
developer of this project so far. Please fill out a ticket in the issue tracker for the project if
you come across a problem or better yet...please contribute better solutions to
problems. I can only do so much and would gladly accept the help when offered. This 
project has taken me a long time to code and I would love to see it flourish. The 
project is currently in Beta. Please use at your own risk and remember that you are responsible
for securing your own servers and infrastructure.

Thank you. Jimwebguru

/**********************************************************/

## Why I created this CMS

I have a long history of full stack web development experience working on many different
custom applications and popular CMS frameworks in different languages. Many people 
have enjoyed using a CMS like Drupal or WordPress but for me the two always seemed to conflict with my style
of building websites. I wanted to create a new CMS that felt better with my style of
development. I decided to build one from scratch in the programming language I
loved the most which was Java. ( I am also looking to port this CMS to PHP with Laravel
and Vue.js )

## Requirements

Experience in Developing Java Web apps is a must

Java application or web server

Mysql (postgres in future)

Maven for compiling and using libraries (if you want to use the existing pom.xml)

This web application uses JSF for the UI and Kotlin for most of the code except for the
JPA classes. Developers do not have to have a knowledge of Kotlin and can add their own
Java classes as needed.

The current code base has been tested on Tomcat 8.5 and Mysql 5.7. The runtime is Java 8.

Popular libraries used in this project are:

- Spring Security
- Bxslider
- Fancybox
- Primefaces and Primefaces Extensions
- JQuery
- SmartMenus
- FlipCard JQuery
- Expanding Grid by Dan Boulet
- Fabric.js
- Servlet code for upload servlets by BalusC
- JSF

## Installation

The codebase for this project is segmented in three different repositories. The Iberis CMS
project gets compiled with the source from all 3 repositories. This was done intentionally 
to keep the pieces modular for people who only wanted to use certain repository code along with 
their own custom code. Keep the source of all three repos in separate directories. You will use the 
directory paths as source directories in your compiling. 

CMS - The web application that utilizes Core Web and Core

[Core Web](https://gitlab.com/jimwebguru/iberis-core-web) - The repository that contains the common web components and admin pages

[Core](https://gitlab.com/jimwebguru/iberis-core) - The repository that contains the JPA classes and some utility classes

[The Iberis DB](https://gitlab.com/jimwebguru/iberis-db) repository stores the starting database file.

There is no easy installer for getting the CMS up and running. Eventually, I would love
to have an installer but for right now its the old school crude way.

Here are the steps to install the CMS:

- Restore the iberis-structure and iberis-starting-data sql db files that are located in the Iberis DB repository to your database server.
- Update the database creds section of the persistence.xml file
- Add the mysql jdbc driver to your application or web server
- Add a new row in the sites database table that includes info about your site. Make the frontPageTemplateId and
inner page template id set to 1.
- Add a new row in the sitememberships table with user id 5, the site id of the new site record, and todays date for signupdate.
- Add a new row in the membershiproles table with the roleid of 1 and sitemembership id of the new row inserted in the sitememberships table.
- Update the settingsValue in each row of the systemsettings table as needed
- Update the cms site id variable in the web.xml file with the id of the new row you added in the sites table 
- Compile the 3 repositories together in this project. The web directory contents in the iberis cms
and iberis core web should move directly in the root of the build artifact. Make sure that all the libraries that are loaded from maven
are compiled into the WEB-INF/lib folder of the build artifact and NOT the classes/lib folder.
There is an IntelliJ IDEA project file in this source to get you started quickly if you are using IntelliJ. You may have to make minor
tweaks to paths and server settings in intellij if you decide to use the IDEA project file.
- Deploy the web app to your java web server or java application server
- Test to make sure that the site loads up

You can login to the backend using the /login context path. Then you would use admin@iberis.com as
the user with password of iberis1234. REMEMBER TO CHANGE THE ADMIN PASSWORD AFTER LOGGING IN!!! You can
do that by navigating to users and updating the admin user.

There may be issues in certain environments where app servers may already include a library in this project. You may need to resolve those conflicts.
JSF for example is one library that is often included in different java app servers.

JAXB has been moved out of Java 11. You will have to add javax.xml.bind artifact from maven repository (pom.xml) if you are trying to use Java 11 runtime.

Please send me a message if the installation does not work for your environment. The readme will be
adjusted for anything that has been discovered in additional installation testing.

## Features Available
There are numerous options for each major feature. There is no way to go over each
and every option. The following are a high level view of the main cms features.

#### Page Layout and Themes
The CMS has been designed for 100% responsive. The screens have been painful tweaked to
be viewable in a screen size as small as 375px by 500px. All images added are by default set
to 100% max width in layouts that use CSS Flexbox grid.

There is a triple inheritance for the JSF facelet templates. The base cms template gets loaded followed by
the base theme template file and then finally the template file that the theme template
file is using for its base structure. The base cms template file loads all the common libraries, base scripting,
base css, etc... before passing on to the theme file.

For example: 

base cms template &gt; animals theme template &gt; 3 box base template file  

Each template has page regions that can accept page item assignments within the cms. The page item
assignments are used for assigning different types of content such as content blocks, blog list,
menus, images, videos, galleries, presentations, etc.. to regions on a page.

You can even scope assignments so that content only shows up when a particular scope context is present.

You will be able to use a layout management view of the web page once you log in. The layout management will
allow you to drag and drop assignments from one region to another. It will also allow you
to easily add additional assignments.

#### Blogs

Write your own blogs and post them. Blogs are organized as Blog and then the entries 
for those blogs. This allows you to you have as many Blogs as you want with their own unique listing
anywhere on the site.

#### Content Pages 

Content can be created as blocks or pages. There are up to 4 content body that can be added 
in different layout options. Content can also be built from other content and laid out with as
many content pieces as needed. There are also many other options to attach files, header images, custom
footers and headers, etc...

You can also create a content record that uses a name of a JSF facelets view file. Typically,
filename.xhtml synatx. This will trigger that the content is pulled by including the file
rather than rendering out the content defined in the content record. You will always want to add
a content record for custom built pages so that the content page can hook into the layouts and
page assignments in the CMS.

#### Presentations

Presentations are content that are provided in a certain structure or functionality.
Presentations can be setup to be the following:
- Dynamic moving sliders
- Accordions
- Side by Side panels with flip card functionality
- Tabbed Pane view
- Expanding Image Grid

#### Galleries

Galleries are similar to presentations but are targeted specifically for images and captions.

Galleries can be the following:
- Sliders with thumbnails
- Image Switch pane
- Fancybox slideshow
- Side by side panels with flip card

#### Images

Images can be uploaded. The uploaded images go to a specific file path defined in the database.
This allows the images to be all contained in one folder path without junking up the website directory.

There are many configuration options for the presentation of images such as:

- Header and footer content
- Overlays
- Dimensions
- Image cropping to create new image

#### Canvases
Canvases can be created and stored based on existing images or text.

For example: You can create a canvas and put 5 images in the canvas. Tweak how the images should look and then
add 5 text boxes in different positions on the canvas. Then generate a new image from the canvas.

The canvas editing is made possible by utilizing the amazing Fabric.js library.

#### Videos
Videos can be added by uploading them or adding custom html. There is also an option to automatically
create the embed html for youtube and vimeo videos by supplying a video url.

There are several video configurations such as:
- Header and Footer content
- Setting the video as background video with overlay
- Setting the video as fixed size or relative size height

#### Menus
There are a wide range of menus you can easily create and customize. 

The menu types are as follows:

- Menu Panel
- Slide Menu
- Tier Menu
- Menubar
- Menu Button
- Mega Menu
- Standard Vertical Menu

Please note that there is an issue with the menu drag and drop for the arranging the
menuitems in mobile. The drag currently does not work in mobile. I am searching for a fix.

#### Icons
A base set of icons is included in case you want to use font icons in custom pages.

Most of the icons are using font icons from font awesome and icomoon.

#### Products and Catalogs
Currently, the cms is geared toward mainly content driven functionality. There are 
some ecommerce pieces also availabe. Products and Product catalogs can be added. 
Product catalogs will display products with the functionality to be able to add them to a
shopping cart. The shopping cart is as far as the ecommerce functionality goes. There is a
shopping cart page but Developers will have to implement their own payment gateway functionality along with custom thank you pages.
