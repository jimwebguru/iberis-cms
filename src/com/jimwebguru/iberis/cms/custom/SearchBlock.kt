package com.jimwebguru.iberis.cms.custom

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class SearchBlock : Serializable
{
	var searchCriteria: String? = null

	var contentList: List<*>? = null

	var searchInitiated = false;

	private val daoUtil = DaoUtil()

	fun submit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.searchCriteria = searchCriteria?.replace(" ","* ")?.toLowerCase()
		this.searchCriteria += "*"
		this.searchInitiated = true

		this.contentList = this.daoUtil.findNativeRange(intArrayOf(0, 50), "SELECT id, title, urlAlias, i.filePath, MATCH(title, header, subHeader, body, body2, body3, body4, footer, subFooter) AGAINST('${this.searchCriteria}' IN BOOLEAN MODE) AS score FROM content o INNER JOIN contentsites cs ON cs.contentId = o.id LEFT JOIN images i ON i.imageid = o.bannerGraphic WHERE cs.siteId = ${cmsSite.site!!.siteID} HAVING score > 0 ORDER BY score DESC")
	}
}