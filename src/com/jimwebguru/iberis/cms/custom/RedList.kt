package com.jimwebguru.iberis.cms.custom

import org.primefaces.model.chart.PieChartModel

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class RedList : Serializable
{
	var chartModel = PieChartModel()

	@PostConstruct
	fun postInit()
	{
		this.chartModel.set("Least Concern", 51)
		this.chartModel.set("Data Deficient", 3)
		this.chartModel.set("Regionally Extinct", 3)
		this.chartModel.set("Critically Endangered", 7)
		this.chartModel.set("Endangered", 11)
		this.chartModel.set("Vulnerable", 15)
		this.chartModel.set("Indeterminate", 2)
		this.chartModel.set("Rare", 4)
		this.chartModel.set("Near Threatened", 3)
		//this.chartModel.setTitle("Red List 2015");
		this.chartModel.legendPosition = "e"
		this.chartModel.isFill = true
		this.chartModel.isShowDataLabels = true
		this.chartModel.diameter = 200
		this.chartModel.seriesColors = "D5F5E3,FFFFFF,000000,E74C3C,E67E22,F39C12,B2BABB,3498DB,F1C40F"
		this.chartModel.extender = "chartExtender"
	}
}
