package com.jimwebguru.iberis.cms.custom

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Image

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DancingBadgers : Serializable
{
	var numberOfBadgers = 0

	var message = "There are " + this.numberOfBadgers + " dancing badgers images!!!"

	var badgerImage: Image? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		this.badgerImage = this.daoUtil.find(Image::class.java, 118L)
	}

	fun submit()
	{
		this.message = "There are " + this.numberOfBadgers + " dancing badgers images!!!"
	}

	fun fakeBadgerArray(): Array<Any?>
	{
		return arrayOfNulls(this.numberOfBadgers)
	}
}
