package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsStats : Serializable
{
	private val daoUtil = DaoUtil()

	var contentCount: Long = 0
	var imagesCount: Long = 0
	var galleriesCount: Long = 0
	var filesCount: Long = 0
	var userCount: Long = 0

	@PostConstruct
	fun postInit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val queryParameter = QueryParameter()
		queryParameter.parameterName = "site"
		queryParameter.parameterValue = cmsSite.site!!.siteID

		val queryParameters = ArrayList<QueryParameter>()
		queryParameters.add(queryParameter)

		this.contentCount = this.daoUtil.count("SELECT COUNT(o) FROM Content o JOIN o.sites s WHERE s.siteID = :site", queryParameters)
		this.imagesCount = this.daoUtil.count(Image::class.java)
		this.galleriesCount = this.daoUtil.count(Gallery::class.java)
		this.filesCount = this.daoUtil.count(File::class.java)
		this.userCount = this.daoUtil.count("SELECT COUNT(o) FROM SiteMembership o WHERE o.site.siteID = " + cmsSite.site!!.siteID!!)
	}
}
