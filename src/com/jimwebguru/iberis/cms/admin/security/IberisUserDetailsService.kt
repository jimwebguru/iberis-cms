package com.jimwebguru.iberis.cms.admin.security

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Role
import com.jimwebguru.iberis.core.persistence.SiteMembership
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException

import java.util.*
import javax.naming.Context
import javax.naming.InitialContext

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class IberisUserDetailsService : UserDetailsService
{
	private val daoUtil = DaoUtil()

	@Throws(UsernameNotFoundException::class)
	override fun loadUserByUsername(userName: String): UserDetails
	{
		val ctx = InitialContext().lookup("java:comp/env") as Context

		val cmsSite = ctx.lookup("cmsSiteId")

		val users = this.daoUtil.find<com.jimwebguru.iberis.core.persistence.User>("SELECT DISTINCT OBJECT(o) FROM User o JOIN o.siteMemberships sm WHERE o.userName = '$userName' AND sm.site.siteID = $cmsSite")

		val user = users!![0]

		var siteMembership: SiteMembership? = null

		for(membership in user.siteMemberships)
		{
			if(membership.site.siteID == cmsSite)
			{
				siteMembership = membership
			}
		}

		val authorities = buildUserAuthority(siteMembership!!.roles)

		return buildUserForAuthentication(user, authorities)

	}

	// Converts com.jimwebguru.iberis.core.persistence.User user to
	// org.springframework.security.core.userdetails.User
	private fun buildUserForAuthentication(user: com.jimwebguru.iberis.core.persistence.User,
	                                       authorities: List<GrantedAuthority>): User
	{
		return User(user.userName, user.password,
				user.enabled!!, true, true, true, authorities)
	}

	private fun buildUserAuthority(userRoles: Collection<Role>): List<GrantedAuthority>
	{
		val setAuths = HashSet<GrantedAuthority>()

		// Build user's authorities
		for (userRole in userRoles)
		{
			setAuths.add(SimpleGrantedAuthority("ROLE_" + userRole.roleName))
		}

		return ArrayList(setAuths)
	}
}
