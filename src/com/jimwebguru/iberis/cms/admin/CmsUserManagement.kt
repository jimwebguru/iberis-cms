package com.jimwebguru.iberis.cms.admin

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.persistence.SiteMembership
import com.jimwebguru.iberis.core.persistence.User
import com.jimwebguru.iberis.core.web.controllers.admin.UserManagement
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.util.ArrayList
import java.util.Date

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsUserManagement : UserManagement()
{
	@PostConstruct
	override fun postInit()
	{
		super.postInit()

		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val queryParameters = ArrayList<QueryParameter>()

		val queryParameter = QueryParameter()
		queryParameter.parameterName = "siteID"
		queryParameter.parameterValue = cmsSite.siteId
		queryParameter.joinClause = "JOIN o.siteMemberships s"
		queryParameter.objectProperty = "s.site.siteID"
		queryParameter.conditionOperator = "="

		queryParameters.add(queryParameter)

		this.genericLazyLoader.additionalQueryParams = queryParameters
	}

	override fun save()
	{
		var createMembership = false

		if (this.user!!.userId == null || this.user!!.userId < 1)
		{
			createMembership = true
		}

		super.save()

		if (this.user!!.userId != null && this.user!!.userId > 0 && createMembership)
		{
			val siteMembership = SiteMembership()
			siteMembership.user = this.user

			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite
			siteMembership.site = this.daoUtil.find(Site::class.java, cmsSite.siteId!!)
			siteMembership.signupDate = Date()

			this.daoUtil.create(siteMembership)

			this.daoUtil.refresh(User::class.java, this.user!!.userId)

			this.user = this.daoUtil.find(User::class.java, this.user!!.userId)
		}
	}
}
