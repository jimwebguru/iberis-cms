package com.jimwebguru.iberis.cms.servlet

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.PageItemAssignment
import com.jimwebguru.iberis.core.persistence.PageRegion
import com.jimwebguru.iberis.core.utils.db.QueryParameter

import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.IOException
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@WebServlet(name = "updateRegionServlet", urlPatterns = arrayOf("/update-region"))
class UpdateRegionServlet : HttpServlet()
{
	private val daoUtil = DaoUtil()

	@Throws(ServletException::class, IOException::class)
	override fun doGet(request: HttpServletRequest, response: HttpServletResponse)
	{
		val assignment = this.daoUtil.find(PageItemAssignment::class.java, request.getParameter("assignmentId")!!.toLong())
		val region = this.daoUtil.find(PageRegion::class.java, request.getParameter("regionId")!!.toLong())
		assignment!!.pageRegion = region
		assignment.sequence = Integer.valueOf(request.getParameter("index"))

		val success = this.daoUtil.update(assignment)

		val message: String

		if (success)
		{
			this.rearrangeAssignmentSequence(assignment)
			message = "Page Item Assignment has been updated."
		}
		else
		{
			message = "Page Item Assignment could not be updated."
		}

		// Init servlet response.
		response.reset()
		response.contentType = "text/html"

		val printWriter = response.writer
		printWriter.write(message)
		printWriter.close()

	}

	private fun rearrangeAssignmentSequence(assignment: PageItemAssignment)
	{
		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = "region"
		queryParameter.parameterValue = assignment.pageRegion.id

		queryParameters.add(queryParameter)

		queryParameter = QueryParameter()
		queryParameter.parameterName = "site"
		queryParameter.parameterValue = assignment.site.siteID

		queryParameters.add(queryParameter)

		val pageItemAssignments = this.daoUtil.find<PageItemAssignment>("SELECT OBJECT(o) FROM PageItemAssignment o WHERE o.site.siteID = :site AND o.pageRegion.id = :region ORDER BY o.sequence ASC", queryParameters)

		var counter = 0

		for (a in pageItemAssignments!!)
		{
			if (a.id != assignment.id)
			{
				if (counter == assignment.sequence)
				{
					counter++
				}

				a.sequence = counter

				this.daoUtil.update(a)

				counter++
			}
		}
	}
}
