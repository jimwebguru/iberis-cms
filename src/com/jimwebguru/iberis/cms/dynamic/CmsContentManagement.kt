package com.jimwebguru.iberis.cms.dynamic

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.web.controllers.cms.DynamicContentManagement
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsContentManagement : DynamicContentManagement()
{
	@PostConstruct
	override fun postInit()
	{
		super.postInit()

		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val queryParameters = ArrayList<QueryParameter>()

		val queryParameter = QueryParameter()
		queryParameter.parameterName = "siteID"
		queryParameter.parameterValue = cmsSite.siteId
		queryParameter.joinClause = "JOIN o.sites s"
		queryParameter.objectProperty = "s.siteID"
		queryParameter.conditionOperator = "="

		queryParameters.add(queryParameter)

		this.loader.additionalQueryParams = queryParameters
	}

	override fun save()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		if (this.content!!.sites == null || this.content!!.sites.isEmpty())
		{
			val sites = ArrayList<Site?>()

			if(cmsSite.site != null)
			{
				sites.add(cmsSite.site)
			}

			this.content!!.sites = sites
		}
		else
		{
			var siteExists = false

			for (site in this.content!!.sites)
			{
				if (site.siteID == cmsSite.site!!.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.content!!.sites.add(cmsSite.site)
			}
		}

		super.save()
	}

	override fun savePageItemAssignment()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.pageItemAssignment.site = cmsSite.site

		super.savePageItemAssignment()
	}
}
