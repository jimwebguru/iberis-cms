package com.jimwebguru.iberis.cms.dynamic

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ProductCatalogView : Serializable
{
	var productCatalog: ProductCatalog? = null

	var lazyLoader: GenericLazyLoader<Product>? = null

	var productGallery: Gallery? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		val urlAlias = GeneralUtil.request!!.getParameter("catalogUrlAlias")

		if (!urlAlias.isNullOrEmpty())
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			val params = ArrayList<QueryParameter>()
			params.add(QueryParameter("alias", urlAlias))
			params.add(QueryParameter("siteId", cmsSite.siteId!!))

			val catalogs = this.daoUtil.find<ProductCatalog>("SELECT OBJECT(o) FROM ProductCatalog o WHERE o.urlAlias = :alias AND o.site.siteID = :siteId", params)

			if (catalogs != null && catalogs.isNotEmpty())
			{
				this.productCatalog = catalogs[0]

				this.lazyLoader = GenericLazyLoader(Product::class.java)

				val queryParameters = ArrayList<QueryParameter>()

				if (this.productCatalog!!.productGroups != null && this.productCatalog!!.productGroups.isNotEmpty())
				{
					var conditionValue = "("

					for (group in this.productCatalog!!.productGroups)
					{
						conditionValue += group.groupId!!.toString() + ","
					}

					conditionValue = conditionValue.substring(0, conditionValue.length - 1) + ")"

					val queryParameter = QueryParameter()

					queryParameter.explicitCondition = "o.productGroup.groupId IN " + conditionValue

					queryParameters.add(queryParameter)

					lazyLoader!!.additionalQueryParams = queryParameters
				}
				else if (this.productCatalog!!.products != null && this.productCatalog!!.products.isNotEmpty())
				{
					var conditionValue = "("

					for (product in this.productCatalog!!.products)
					{
						conditionValue += product.productId!!.toString() + ","
					}

					conditionValue = conditionValue.substring(0, conditionValue.length - 1) + ")"

					val queryParameter = QueryParameter()

					queryParameter.explicitCondition = "o.productId IN " + conditionValue

					queryParameters.add(queryParameter)

					lazyLoader!!.additionalQueryParams = queryParameters
				}
			}
		}
	}

	fun createProductGallery(product: Product)
	{
		this.productGallery = Gallery()

		this.productGallery!!.galleryImages = ArrayList()

		if (product.productImages != null && product.productImages.isNotEmpty())
		{
			for (pImage in product.productImages)
			{
				val galleryImage = GalleryImage()
				galleryImage.gallery = this.productGallery
				galleryImage.image = pImage.image

				this.productGallery!!.galleryImages.add(galleryImage)
			}
		}
	}
}
