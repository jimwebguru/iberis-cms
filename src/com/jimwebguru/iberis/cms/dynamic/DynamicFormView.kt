package com.jimwebguru.iberis.cms.dynamic

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.XmlUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.MailUtil
import org.joda.time.DateTime
import org.primefaces.PrimeFaces
import org.primefaces.json.JSONArray
import org.primefaces.json.JSONObject

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList
import java.util.Hashtable
import javax.mail.Address
import javax.mail.internet.InternetAddress

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DynamicFormView : Serializable
{
	var form: DynamicForm? = null

	private val daoUtil = DaoUtil()

	var site: Site? = null

	@PostConstruct
	fun postInit()
	{
		val urlAlias = GeneralUtil.request!!.getParameter("formUrlAlias")

		if (!urlAlias.isNullOrEmpty())
		{
			val params = ArrayList<QueryParameter>()
			params.add(QueryParameter("alias", urlAlias))

			val forms = this.daoUtil.find<DynamicForm>("SELECT OBJECT(o) FROM DynamicForm o WHERE o.urlAlias = :alias", params)

			if (forms != null && forms.isNotEmpty())
			{
				this.form = forms[0]
			}
		}

		val cmsSiteId = GeneralUtil.getWebXmlEnvParam("cmsSiteId")

		if (cmsSiteId != null)
		{
			this.site = this.daoUtil.find(Site::class.java, cmsSiteId as Long)
		}
	}

	fun submitForm()
	{
		val mailUtil = GeneralUtil.getManagedBean("mailUtil") as MailUtil

		var submitted = true

		if(this.form!!.sendEmail)
		{
			var toEmail: Array<Address>? = null

			if (!this.form!!.email.isNullOrEmpty())
			{
				toEmail = arrayOf(InternetAddress(this.form!!.email)) as Array<Address>
			}

			if (this.form!!.useSiteEmail && !this.site!!.mailFrom.isNullOrEmpty())
			{
				toEmail = arrayOf(InternetAddress(this.site!!.mailFrom)) as Array<Address>
			}

			if (!toEmail.isNullOrEmpty())
			{
				submitted = mailUtil.sendEmail(toEmail, null, null, this.form!!.emailSubject, processEmailTemplate(this.form!!), true)
			}
		}

		if(this.form!!.sendConfirmationEmail && this.form!!.recipientEmailField != null)
		{
			submitted = mailUtil.sendEmail(arrayOf(InternetAddress(getFormFieldValue(this.form, this.form!!.recipientEmailField.id))), null, null, this.form!!.emailSubject, this.form!!.confirmationEmailTemplate, true)
		}

		if(submitted)
		{
			this.createFormSubmission(this.form)

			GeneralUtil.addMessage(this.form!!.confirmation, FacesMessage.SEVERITY_INFO)
		}
	}

	fun submitDynamicForm()
	{
		val mailUtil = GeneralUtil.getManagedBean("mailUtil") as MailUtil

		val request = GeneralUtil.request

		val formId = request!!.getParameter("formId")!!.toLong()
		val fieldData = request.getParameter("fieldData")
		val fieldsContainerName = request.getParameter("fieldsContainerName")

		val dForm = this.daoUtil.find(DynamicForm::class.java, formId)

		val fieldValues = Hashtable<String, Array<String?>>()

		val jsonArray = JSONArray(fieldData)

		for (i in 0..jsonArray.length() - 1)
		{
			val jsonObject = jsonArray.get(i) as JSONObject

			val fieldArray = arrayOfNulls<String>(2)

			val name = jsonObject.getString("name")

			fieldArray[0] = jsonObject.getString("value")

			if (jsonObject.has("displayValue"))
			{
				fieldArray[1] = jsonObject.getString("displayValue")
			}

			fieldValues.put(name.substring(name.indexOf("-") + 1, name.indexOf("_")), fieldArray)
		}

		for (section in dForm!!.formSections)
		{
			for (field in section.fields)
			{
				if (fieldValues.containsKey(field.id!!.toString()))
				{
					val fieldArray = fieldValues.get(field.id!!.toString()) as Array<String?>

					field.value = fieldArray[0]

					if (fieldArray[1] != null)
					{
						field.displayValue = fieldArray[1]
					}
				}
			}
		}

		var submitted = true

		if(dForm.sendEmail)
		{
			var toEmail: Array<Address>? = null

			if (!dForm.email.isNullOrEmpty())
			{
				toEmail = arrayOf(InternetAddress(dForm.email)) as Array<Address>
			}

			if (dForm.useSiteEmail && !this.site!!.mailFrom.isNullOrEmpty())
			{
				toEmail = arrayOf(InternetAddress(this.site!!.mailFrom)) as Array<Address>
			}

			if (!toEmail.isNullOrEmpty())
			{
				submitted = mailUtil.sendEmail(toEmail, null, null, dForm.emailSubject, processEmailTemplate(dForm), true)
			}
		}

		if(dForm.sendConfirmationEmail && dForm.recipientEmailField != null)
		{
			submitted = mailUtil.sendEmail(arrayOf(InternetAddress(getFormFieldValue(dForm, dForm.recipientEmailField.id))), null, null, dForm.emailSubject, dForm.confirmationEmailTemplate, true)
		}

		if (submitted)
		{
			this.createFormSubmission(dForm)

			PrimeFaces.current().executeScript("$('div[id$=$fieldsContainerName]').hide();$('#$fieldsContainerName-error').hide();$('#$fieldsContainerName-confirmation').show();")
		}
		else
		{
			PrimeFaces.current().executeScript("$('#$fieldsContainerName-error').show();")
		}
	}

	private fun processEmailTemplate(dForm: DynamicForm?): String
	{
		if(dForm != null)
		{
			var submittedFieldsHtml = "<div>"
			var processedEmailTemplate = dForm.emailTemplate

			if (dForm.formSections != null && dForm.formSections.isNotEmpty())
			{
				for (section in dForm.formSections)
				{
					if (section.fields != null && section.fields.isNotEmpty())
					{
						for (field in section.fields)
						{
							processedEmailTemplate = processedEmailTemplate.replace("[%${field.name}%]", field.value)
							submittedFieldsHtml += "<div><b>${field.displayValue}:</b> ${field.value}</div>"
						}
					}
				}
			}

			submittedFieldsHtml += "</div>"

			processedEmailTemplate = processedEmailTemplate.replace("[%ALL_FIELDS%]", submittedFieldsHtml)

			return processedEmailTemplate
		}

		return ""
	}

	private fun getFormFieldValue(dForm: DynamicForm?, fieldId: Long): String
	{
		if(dForm != null)
		{
			if (dForm.formSections != null && dForm.formSections.isNotEmpty())
			{
				for (section in dForm.formSections)
				{
					if (section.fields != null && section.fields.isNotEmpty())
					{
						for (field in section.fields)
						{
				 	        if(field.id == fieldId)
				            {
					            return field.value
				            }
						}
					}
				}
			}
		}

		return ""
	}

	private fun createFormSubmission(dForm: DynamicForm?): Boolean
	{
		val submission = FormSubmission()

		val sb = StringBuilder()

		sb.append("<submission>")
		sb.append("<title>")
		sb.append(dForm!!.title)
		sb.append("</title>")

		if (dForm.formSections != null && dForm.formSections.isNotEmpty())
		{
			sb.append("<sections>")

			for (section in dForm.formSections)
			{
				sb.append("<section>")
				sb.append("<title>")
				sb.append(section.title)
				sb.append("</title>")

				if (section.fields != null && section.fields.isNotEmpty())
				{
					sb.append("<fields>")

					for (field in section.fields)
					{
						sb.append("<field>")
						sb.append("<label>")
						sb.append(field.label)
						sb.append("</label>")
						sb.append("<value>")

						if (field.fieldType.typeName == "LOOKUP")
						{
							sb.append(field.displayValue)
						}
						else
						{
							sb.append(field.value)
						}

						sb.append("</value>")
						sb.append("</field>")
					}

					sb.append("</fields>")
				}

				sb.append("</section>")
			}

			sb.append("</sections>")
		}

		sb.append("</submission>")

		submission.form = dForm
		submission.created = DateTime.now().toDate()

		val xmlUtil = XmlUtil()
		submission.submission = xmlUtil.prettyPrint(sb.toString())

		val submitted = this.daoUtil.create(submission)

		dForm.submitted = submitted

		return submitted
	}
}
