package com.jimwebguru.iberis.cms.dynamic

import com.jimwebguru.iberis.cms.CmsSite
import com.jimwebguru.iberis.core.persistence.Content
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Note
import com.jimwebguru.iberis.core.persistence.UrlRedirect
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.ocpsoft.pretty.PrettyContext

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.text.Normalizer
import java.util.ArrayList
import javax.faces.context.FacesContext

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DynamicContent : Serializable
{
	var content: Content? = null

	var notes: List<Note>? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		val urlAlias = GeneralUtil.request!!.getParameter("urlAlias")

		if (!urlAlias.isNullOrEmpty())
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			val redirectSent = this.processRedirects(cmsSite)

			if (!redirectSent)
			{
				val params = ArrayList<QueryParameter>()
				params.add(QueryParameter("alias", urlAlias))
				params.add(QueryParameter("type", "block"))
				params.add(QueryParameter("siteId", cmsSite.siteId!!))

				val contentItems = this.daoUtil.find<Content>("SELECT DISTINCT OBJECT(o) FROM Content o JOIN o.sites s WHERE o.urlAlias = :alias AND o.type != :type AND s.siteID = :siteId", params)

				if (contentItems != null && contentItems.isNotEmpty())
				{
					this.content = contentItems[0]

					if (this.content!!.showNotes != null && this.content!!.showNotes)
					{
						val queryParameters = ArrayList<QueryParameter>()

						var queryParameter = QueryParameter("systemUnit", 62L)
						queryParameters.add(queryParameter)

						queryParameter = QueryParameter("instanceId", this.content!!.id)
						queryParameters.add(queryParameter)

						this.notes = this.daoUtil.find("SELECT OBJECT(o) FROM Note o WHERE o.systemUnit.unitID = :systemUnit AND o.instanceId = :instanceId ORDER BY o.id", queryParameters)
					}
				}
				else
				{
					FacesContext.getCurrentInstance().externalContext.redirect(FacesContext.getCurrentInstance().externalContext.requestContextPath + "/404")
				}
			}
		}
		else
		{
			FacesContext.getCurrentInstance().externalContext.redirect(FacesContext.getCurrentInstance().externalContext.requestContextPath + "/404")
		}
	}

	/**
	 * Added for db stored url redirect when template loads. Added here so the logic
	 * runs once when the template is loading. Unfortunately, this logic would have executed
	 * multiple times if it was filter and there is no way to get the prettyfaces url
	 * in a filter cuz prettyfaces loads after the filter. Also wanted the logic to only run when
	 * all urls of the front end site was loading and not the admin portion. Logic can be moved to a
	 * better place once a better solution is provided.
	 */
	private fun processRedirects(cmsSite : CmsSite) : Boolean
	{
		var urlPath = PrettyContext.getCurrentInstance().requestURL.toString()

		if(urlPath == "")
		{
			urlPath = "/"
		}

		if(!urlPath.startsWith("/"))
		{
			urlPath = "/" + urlPath
		}

		var urlPaths = urlPath.split("/").filter{ it.isNotEmpty() }

		var queryPart = ""

		if(urlPaths.isNotEmpty())
		{
			urlPaths = urlPaths.subList(0, urlPaths.lastIndex)

			var queryPath = "/"

			for (urlPart: String in urlPaths)
			{
				queryPath += urlPart + "/"
				queryPart += " OR o.redirectFrom = '$queryPath*'"
			}
		}

		val query = "SELECT OBJECT(o) FROM UrlRedirect o WHERE o.site.siteID = ${cmsSite.site!!.siteID} AND (o.redirectFrom = '$urlPath' $queryPart) AND o.enabled = 1"

		val redirectList = this.daoUtil.find<UrlRedirect>(query)

		if(redirectList != null && redirectList.isNotEmpty())
		{
			val redirect = redirectList[0]

			if (redirect.redirectTo.startsWith("/"))
			{
				FacesContext.getCurrentInstance().externalContext.redirect(FacesContext.getCurrentInstance().externalContext.requestContextPath + redirect.redirectTo)
			}
			else
			{
				FacesContext.getCurrentInstance().externalContext.redirect(redirect.redirectTo)
			}

			return true
		}

		return false
	}

	val title: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.title)
			}
			else
			{
				""
			}
		}

	val body: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.body, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val body2: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.body2, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val body3: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.body3, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val body4: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.body4, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val header: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.header, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val subHeader: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.subHeader, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val footer: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.footer, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val subFooter: String
		get()
		{
			return if (this.content != null)
			{
				GeneralUtil.cleanHTML(this.content?.subFooter, this.content?.allowEditorStyles!!)
			}
			else
			{
				""
			}
		}

	val wrapperClass: String
		get()
		{
			return if (this.content != null)
			{
				Normalizer.normalize(this.content!!.title?.toLowerCase(), Normalizer.Form.NFD)
						.replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "")
						.replace("[^\\p{Alnum}]+".toRegex(), "-")
			}
			else
			{
				""
			}
		}
}
