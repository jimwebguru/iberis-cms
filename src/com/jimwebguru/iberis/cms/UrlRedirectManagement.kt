package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.UrlRedirect
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.RowEditEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class UrlRedirectManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var urlRedirect = UrlRedirect()
	var selectedRedirect: UrlRedirect? = null

	var lazyLoader = GenericLazyLoader(UrlRedirect::class.java)

	@PostConstruct
	fun postInitialization()
	{

	}

	fun addRedirect()
	{
		if (this.urlRedirect.redirectFrom != null && this.urlRedirect.redirectFrom.isNotEmpty()
				&& this.urlRedirect.redirectTo != null && this.urlRedirect.redirectTo.isNotEmpty())
		{
			var cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			this.urlRedirect.site = cmsSite.site
			this.urlRedirect.enabled = true

			if(!this.urlRedirect.redirectFrom.startsWith("/"))
			{
				this.urlRedirect.redirectFrom = "/" + this.urlRedirect.redirectFrom
			}

			val success = this.daoUtil.create(this.urlRedirect)

			if (success)
			{
				this.urlRedirect = UrlRedirect()

				GeneralUtil.addMessage("Redirect added.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Could not add Redirect.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun delete(item: UrlRedirect)
	{
		val success = this.daoUtil.delete(item)

		if (success)
		{
			GeneralUtil.addMessage("Redirect deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Could not delete Redirect.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.selectedRedirect = event.`object` as UrlRedirect
	}

	fun onRowEdit(event: RowEditEvent)
	{
		val redirect: UrlRedirect = event.`object` as UrlRedirect

		if(!redirect.redirectFrom.startsWith("/"))
		{
			redirect.redirectFrom = "/" + redirect.redirectFrom
		}

		val success = this.daoUtil.update(redirect)

		if (success)
		{
			GeneralUtil.addMessage("Redirect modified.", FacesMessage.SEVERITY_INFO)
		}
	}

	@SuppressWarnings("unused")
	fun onRowCancel(event: RowEditEvent)
	{

	}
}
