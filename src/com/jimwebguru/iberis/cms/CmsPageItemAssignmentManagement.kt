package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.persistence.PageItemAssignment
import com.jimwebguru.iberis.core.persistence.PageRegion
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.controllers.cms.PageItemAssignmentManagement
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsPageItemAssignmentManagement : PageItemAssignmentManagement()
{
	val pageItemFilter: String
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			if (this.pageItemAssignment?.pageItem != null && this.pageItemAssignment?.pageItem == 62L)
			{
				return "LEFT JOIN o.sites AS s WHERE o.availableGlobal = TRUE OR s.siteID = " + cmsSite.site?.siteID!!
			}
			else if("061|062|065|066|033|075|083|089|090".contains(this.pageItemAssignment?.pageItem.toString()))
			{
				return "LEFT JOIN o.sites AS s WHERE s.siteID = " + cmsSite.site?.siteID!!
			}

			return ""
		}

	@PostConstruct
	override fun postInit()
	{
		super.postInit()

		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val queryParameters = ArrayList<QueryParameter>()

		val queryParameter = QueryParameter()
		queryParameter.parameterName = "siteID"
		queryParameter.parameterValue = cmsSite.siteId
		queryParameter.objectProperty = "site.siteID"

		queryParameters.add(queryParameter)

		this.lazyLoader.additionalQueryParams = queryParameters

		val regionId = GeneralUtil.getQueryStringValue("regionId")

		if (regionId != null && regionId.isNotEmpty())
		{
			val region = this.daoUtil.find(PageRegion::class.java, regionId.toLong())

			if (this.pageItemAssignment == null)
			{
				this.pageItemAssignment = PageItemAssignment()
				this.pageItemAssignment?.pageRegion = region

				val index = GeneralUtil.getQueryStringValue("sequence")

				if (index != null)
				{
					this.pageItemAssignment?.sequence = Integer.parseInt(index)
				}
			}
		}
	}

	override fun savePageItemAssignment()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.pageItemAssignment?.site = cmsSite.site

		super.savePageItemAssignment()
	}
}
