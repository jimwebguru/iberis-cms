package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.cms.dynamic.DynamicContent
import com.jimwebguru.iberis.cms.dynamic.DynamicFormView
import com.jimwebguru.iberis.core.persistence.Layout
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object PageUtil
{
	val pageObjectLayout: Layout?
		get()
		{
			val request = GeneralUtil.request

			val contentUrlAlias = request!!.getParameter("urlAlias")

			if (contentUrlAlias != null)
			{
				val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

				return content?.pageTemplate
			}

			val formUrlAlias = request.getParameter("formUrlAlias")

			if (formUrlAlias != null)
			{
				val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

				return form?.pageTemplate
			}

			return null
		}

	val pageObjectScope: String?
		get()
		{
			val request = GeneralUtil.request

			val contentUrlAlias = request!!.getParameter("urlAlias")

			if (contentUrlAlias != null)
			{
				val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

				return content?.scope
			}

			/*val formUrlAlias = request.getParameter("formUrlAlias")

			if (formUrlAlias != null)
			{
				val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

				return form?.scope
			} */

			return null
		}

	val allowOnlyScope: Boolean
		get()
		{
			val request = GeneralUtil.request

			val contentUrlAlias = request!!.getParameter("urlAlias")

			if (contentUrlAlias != null)
			{
				val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

				if(content != null)
				{
					return content.showOnlyScope
				}
			}

			/*val formUrlAlias = request.getParameter("formUrlAlias")

			if (formUrlAlias != null)
			{
				val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

				return form!!.showOnlyScope
			} */

			return false
		}

	fun pageObjectExists(): Boolean
	{
		val request = GeneralUtil.request

		return (request!!.getParameter("urlAlias") != null || request.getParameter("formUrlAlias") != null || request.getParameter("blogEntryUrlAlias") != null)
	}
}
