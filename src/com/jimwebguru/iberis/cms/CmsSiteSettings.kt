package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsSiteSettings : Serializable
{
	var site: Site? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.site = this.daoUtil.find(Site::class.java, cmsSite.siteId!!)

		if (this.site!!.siteType == null)
		{
			this.site!!.siteType = SiteType()
		}

		if (this.site!!.frontPageContent == null)
		{
			this.site!!.frontPageContent = Content()
		}

		if (this.site!!.logo == null)
		{
			this.site!!.logo = Image()
		}

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property
			val frontPageLayout = Layout()
			frontPageLayout.id = this.site!!.frontPageTemplate.id
			frontPageLayout.name = this.site!!.frontPageTemplate.name
			this.site!!.frontPageTemplate = frontPageLayout
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property
			val innerLayout = Layout()
			innerLayout.id = this.site!!.innerPageTemplate.id
			innerLayout.name = this.site!!.innerPageTemplate.name
			this.site!!.innerPageTemplate = innerLayout
		}

	}

	fun saveSiteSettings()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.clearObjectForSave()

		if (this.daoUtil.update(this.site!!))
		{
			GeneralUtil.addMessage("Site settings has been updated.", FacesMessage.SEVERITY_INFO)

			cmsSite.reloadSite()
		}
		else
		{
			GeneralUtil.addMessage("Site settings could not be updated.", FacesMessage.SEVERITY_ERROR)
		}

		this.resetObjectForUIBind()
	}

	private fun resetObjectForUIBind()
	{
		if (this.site!!.siteType == null)
		{
			this.site!!.siteType = SiteType()
		}

		if (this.site!!.frontPageContent == null)
		{
			this.site!!.frontPageContent = Content()
		}

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}

		if (this.site!!.logo == null)
		{
			this.site!!.logo = Image()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.site!!.siteType != null && (this.site!!.siteType == null || this.site!!.siteType.typeID == 0L))
		{
			this.site!!.siteType = null
		}

		if (this.site!!.frontPageContent != null && (this.site!!.frontPageContent.id == null || this.site!!.frontPageContent.id == 0L))
		{
			this.site!!.frontPageContent = null
		}

		if (this.site!!.frontPageTemplate != null && (this.site!!.frontPageTemplate.id == null || this.site!!.frontPageTemplate.id == 0L))
		{
			this.site!!.frontPageTemplate = null
		}

		if (this.site!!.innerPageTemplate != null && (this.site!!.innerPageTemplate.id == null || this.site!!.innerPageTemplate.id == 0L))
		{
			this.site!!.innerPageTemplate = null
		}

		if (this.site!!.logo != null && (this.site!!.logo.imageid == null || this.site!!.logo.imageid < 1))
		{
			this.site!!.logo = null
		}
	}
}
