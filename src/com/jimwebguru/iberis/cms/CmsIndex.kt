package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsIndex : Serializable
{
	var recentContent: List<Content>? = null
	var recentImages: List<Image>? = null
	var recentGalleries: List<Gallery>? = null
	var recentPresentations: List<Presentation>? = null
	var recentVideos: List<Video>? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.recentContent = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Content o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} ORDER BY o.id DESC")
		this.recentImages = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Image o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} ORDER BY o.imageid DESC")
		this.recentGalleries = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Gallery o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} ORDER BY o.id DESC")
		this.recentPresentations = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Presentation o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} ORDER BY o.id DESC")
		this.recentVideos = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Video o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} ORDER BY o.id DESC")
	}
}
