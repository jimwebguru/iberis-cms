package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.persistence.SiteMembership
import com.jimwebguru.iberis.core.persistence.User
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.*
import javax.faces.application.FacesMessage

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class Registration : Serializable
{
	var user = User()

	private val daoUtil = DaoUtil()

	fun register(): String
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		if (cmsSite.site!!.emailAsUsername!!)
		{
			this.user.userName = this.user.email
		}

		if(cmsSite.site!!.allowRegistration)
		{
			val userCount = this.daoUtil.count("SELECT COUNT(o) FROM User o WHERE o.userName = '" + this.user.userName + "'")

			if (userCount == 0L)
			{
				val success = this.daoUtil.create(this.user)

				if (success)
				{
					val siteMembership = SiteMembership()
					siteMembership.user = this.user
					siteMembership.site = this.daoUtil.find(Site::class.java, cmsSite.siteId!!) as Site
					siteMembership.signupDate = Date()

					this.daoUtil.create(siteMembership)

					this.daoUtil.refresh(User::class.java, this.user.userId)

					return "pretty:registration-confirmation"
				}
				else
				{
					GeneralUtil.addMessage("Could not register your account.", FacesMessage.SEVERITY_ERROR);
				}
			}
			else
			{
				GeneralUtil.addMessage("Username already taken.", FacesMessage.SEVERITY_ERROR);
			}
		}
		else
		{
			GeneralUtil.addMessage("Registration is not enabled.", FacesMessage.SEVERITY_ERROR);
		}

		return ""
	}
}
