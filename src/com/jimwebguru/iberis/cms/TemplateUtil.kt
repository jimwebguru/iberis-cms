package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.cms.dynamic.DynamicContent
import com.jimwebguru.iberis.cms.dynamic.DynamicFormView
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil.request
import com.ocpsoft.pretty.PrettyContext

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import javax.faces.context.FacesContext

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class TemplateUtil : Serializable
{
	private val daoUtil = DaoUtil()

	var pageTemplate: Layout? = null
	var contentId: Long? = null
	var formId: Long? = null

	var site: Site? = null

	val baseTemplateFolder: String
		get()
		{
			if (PageUtil.pageObjectExists())
			{
				val pageLayout = PageUtil.pageObjectLayout

				return if (pageLayout != null)
				{
					if (pageLayout.templateDirName.isNotEmpty())
					{
						pageLayout.templateDirName
					}
					else
					{
						pageTemplateByContext
					}
				}
				else
				{
					pageTemplateByContext
				}
			}
			else
			{
				return pageTemplateByContext
			}
		}

	val mainContentTitle: String
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					return "Content (" + content!!.id + "): " + content.title
				}

				val formUrlAlias = request.getParameter("formUrlAlias")

				if (formUrlAlias != null)
				{
					val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

					return "Dynamic Form (" + form!!.id + "): " + form.title
				}
			}

			if (PrettyContext.getCurrentInstance().requestURL.toString() == "/" || PrettyContext.getCurrentInstance().requestURL.toString().isEmpty())
			{
				if (cmsSite.site!!.showFrontPageContent && cmsSite.site!!.frontPageContent != null)
				{
					return cmsSite.site!!.frontPageContent.title
				}
			}

			return "Main Page Content"
		}

	val mainContentDebugEditLink: String
		get()
		{
			var editLink = ""
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					if(content!!.scope != null && content.scope.isNotEmpty())
					{
						editLink += " ----- Content Scope Tag: ${content.scope}"
					}

					editLink += "<a href=\"/cms/content-management/form/${content.id}\"><span class=\"icon-edit\"></span></a>"
				}

				val formUrlAlias = request.getParameter("formUrlAlias")

				if (formUrlAlias != null)
				{
					val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

					return "<a href=\"/cms/dynamic-form-management/form/${form!!.id}\"><span class=\"icon-edit\"></span></a>"
				}
			}

			if (PrettyContext.getCurrentInstance().requestURL.toString() == "/" || PrettyContext.getCurrentInstance().requestURL.toString().isEmpty())
			{
				if (cmsSite.site!!.frontPageContent != null)
				{
					if(cmsSite.site!!.frontPageContent!!.scope != null && cmsSite.site!!.frontPageContent.scope.isNotEmpty())
					{
						editLink += " ----- Content Scope Tag: ${cmsSite.site!!.frontPageContent.scope} "
					}

					editLink += "<a href=\"/cms/content-management/form/${cmsSite.site!!.frontPageContent.id}\"><span class=\"icon-edit\"></span></a>"
				}
			}

			return editLink
		}

	val pageMetaTitle: String?
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite
			var metaTitle: String? = null

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					metaTitle = content?.metaTitle
				}

				val formUrlAlias = request.getParameter("formUrlAlias")

				if (formUrlAlias != null)
				{
					val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

					metaTitle = form?.metaTitle
				}
				
				val blogEntryUrlAlias = request.getParameter("blogEntryUrlAlias")
				
				if(blogEntryUrlAlias != null)
				{
					val blogEntry = (GeneralUtil.getManagedBean("blogView") as BlogView).blogEntry
					
					metaTitle = blogEntry?.metaTitle
				}
			}
			else
			{
				metaTitle = cmsSite.site!!.metaTitle
			}

			if (metaTitle == null || metaTitle.isEmpty())
			{
				metaTitle = cmsSite.site!!.siteName
			}

			return metaTitle
		}

	val pageMetaDescription: String?
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite
			var metaDescription: String? = null

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					metaDescription = content?.metaDescription
				}


				val formUrlAlias = request.getParameter("formUrlAlias")

				if (formUrlAlias != null)
				{
					val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

					metaDescription = form?.metaDescription
				}
				
				val blogEntryUrlAlias = request.getParameter("blogEntryUrlAlias")
				
				if(blogEntryUrlAlias != null)
				{
					val blogEntry = (GeneralUtil.getManagedBean("blogView") as BlogView).blogEntry
					
					metaDescription = blogEntry?.metaDescription
				}

				if (metaDescription == null || metaDescription.isEmpty())
				{
					metaDescription = cmsSite.site!!.metaDescription
				}
			}
			else
			{
				metaDescription = cmsSite.site!!.metaDescription
			}

			return metaDescription
		}

	val socialShareMeta: String?
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite
			var shareMeta = "<meta property=\"og:type\" content=\"website\"/>"

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					if(content!!.socialTitle != null && content.socialTitle.isNotEmpty())
					{
						shareMeta += "<meta property=\"og:title\" content=\"${content.socialTitle}\"/>"
					}
					else
					{
						shareMeta += "<meta property=\"og:title\" content=\"${cmsSite.site?.metaTitle}\"/>"
					}

					if(content.socialDescription != null && content.socialDescription.isNotEmpty())
					{
						shareMeta += "<meta property=\"og:description\" content=\"${content.socialDescription}\"/>"
					}
					else
					{
						shareMeta += "<meta property=\"og:description\" content=\"${cmsSite.site?.metaDescription}\"/>"
					}

					shareMeta += "<meta property=\"og:url\" content=\"${cmsSite.siteUrl}/content/${content.urlAlias}\"/>"

					if(content.bannerGraphic != null)
					{
						shareMeta += "<meta property=\"og:image\" content=\"${cmsSite.siteUrl}${content.bannerGraphic.filePath}\"/>"
						
						if(content.bannerGraphic.width > 0)
						{
							shareMeta += "<meta property=\"og:image:width\" content=\"${content.bannerGraphic.width}\"/>"
						}
						
						if(content.bannerGraphic.height > 0)
						{
							shareMeta += "<meta property=\"og:image:height\" content=\"${content.bannerGraphic.height}\"/>"
						}
					}
				}
				
				val blogEntryUrlAlias = request.getParameter("blogEntryUrlAlias")
				
				if(blogEntryUrlAlias != null)
				{
					val blogEntry = (GeneralUtil.getManagedBean("blogView") as BlogView).blogEntry
					
					if(blogEntry!!.socialTitle != null && blogEntry.socialTitle.isNotEmpty())
					{
						shareMeta += "<meta property=\"og:title\" content=\"${blogEntry.socialTitle}\"/>"
					}
					else
					{
						shareMeta += "<meta property=\"og:title\" content=\"${cmsSite.site?.metaTitle}\"/>"
					}

					if(blogEntry.socialDescription != null && blogEntry.socialDescription.isNotEmpty())
					{
						shareMeta += "<meta property=\"og:description\" content=\"${blogEntry.socialDescription}\"/>"
					}
					else
					{
						shareMeta += "<meta property=\"og:description\" content=\"${cmsSite.site?.metaDescription}\"/>"
					}

					shareMeta += "<meta property=\"og:url\" content=\"${cmsSite.siteUrl}/blog/${blogEntry.blog.urlAlias}/${blogEntry.urlAlias}\"/>"

					if(blogEntry.headerImage != null)
					{
						shareMeta += "<meta property=\"og:image\" content=\"${cmsSite.siteUrl}${blogEntry.headerImage.filePath}\"/>"
						
						if(blogEntry.headerImage.width > 0)
						{
							shareMeta += "<meta property=\"og:image:width\" content=\"${blogEntry.headerImage.width}\"/>"
						}
						
						if(blogEntry.headerImage.height > 0)
						{
							shareMeta += "<meta property=\"og:image:height\" content=\"${blogEntry.headerImage.height}\"/>"
						}
					}
					else if(blogEntry.bodyImage != null)
					{
						shareMeta += "<meta property=\"og:image\" content=\"${cmsSite.siteUrl}${blogEntry.bodyImage.filePath}\"/>"
						
						if(blogEntry.bodyImage.width > 0)
						{
							shareMeta += "<meta property=\"og:image:width\" content=\"${blogEntry.bodyImage.width}\"/>"
						}
						
						if(blogEntry.bodyImage.height > 0)
						{
							shareMeta += "<meta property=\"og:image:height\" content=\"${blogEntry.bodyImage.height}\"/>"
						}
					}
				}
			}
			else
			{
				shareMeta += "<meta property=\"og:title\" content=\"${cmsSite.site?.metaTitle}\"/>"
				shareMeta += "<meta property=\"og:description\" content=\"${cmsSite.site?.metaDescription}\"/>"
				shareMeta += "<meta property=\"og:url\" content=\"${cmsSite.siteUrl}\"/>"
			}

			return shareMeta
		}

	val centeredRegions: Boolean
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			if (PageUtil.pageObjectExists())
			{
				val request = GeneralUtil.request

				val contentUrlAlias = request!!.getParameter("urlAlias")

				if (contentUrlAlias != null)
				{
					val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

					if(content!!.overrideRegions)
					{
						return content.centerRegions
					}
				}
			}

			return cmsSite.site!!.centerRegions
		}

	private val pageTemplateByContext: String
		get()
		{
			val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

			return if (PrettyContext.getCurrentInstance().requestURL.toString() == "/" || PrettyContext.getCurrentInstance().requestURL.toString().isEmpty())
			{
				cmsSite.site!!.frontPageTemplate.templateDirName
			}
			else
			{
				cmsSite.site!!.innerPageTemplate.templateDirName
			}
		}

	val requestUrl: String
		get() = PrettyContext.getCurrentInstance().requestURL.toString()

	@PostConstruct
	fun postInit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		this.site = this.daoUtil.find(Site::class.java, cmsSite.siteId!!)

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property
			val frontPageLayout = Layout()
			frontPageLayout.id = this.site!!.frontPageTemplate.id
			frontPageLayout.name = this.site!!.frontPageTemplate.name
			this.site!!.frontPageTemplate = frontPageLayout
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property
			val innerLayout = Layout()
			innerLayout.id = this.site!!.innerPageTemplate.id
			innerLayout.name = this.site!!.innerPageTemplate.name
			this.site!!.innerPageTemplate = innerLayout
		}

		if (PageUtil.pageObjectExists())
		{
			val request = GeneralUtil.request

			val contentUrlAlias = request!!.getParameter("urlAlias")

			if (contentUrlAlias != null)
			{
				val content = (GeneralUtil.getManagedBean("dynamicContent") as DynamicContent).content

				this.contentId = content?.id
			}

			val formUrlAlias = request.getParameter("formUrlAlias")

			if (formUrlAlias != null)
			{
				val form = (GeneralUtil.getManagedBean("dynamicFormView") as DynamicFormView).form

				this.formId = form?.id
			}

			val pageLayout = PageUtil.pageObjectLayout

			if (pageLayout != null)
			{
				this.pageTemplate = this.daoUtil.find(Layout::class.java, pageLayout.id)
			}
			else
			{
				this.pageTemplate = Layout()
			}
		}
	}

	fun saveTemplateSetting()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		if (this.site!!.frontPageTemplate != null && this.site!!.frontPageTemplate.id == 0L)
		{
			this.site!!.frontPageTemplate = null
		}

		if (this.site!!.innerPageTemplate != null && this.site!!.innerPageTemplate.id == 0L)
		{
			this.site!!.innerPageTemplate = null
		}

		this.daoUtil.update(this.site!!)

		cmsSite.reloadSite()

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}

		if (this.contentId != null)
		{
			val content = this.daoUtil.find(Content::class.java, this.contentId!!)

			if (this.pageTemplate != null && this.pageTemplate!!.id != null && this.pageTemplate!!.id > 0)
			{
				if (content != null)
				{
					content.pageTemplate = this.pageTemplate
				}
			}
			else
			{
				content!!.pageTemplate = null
			}

			this.daoUtil.update(content!!)
		}

		if (this.formId != null)
		{
			val form = this.daoUtil.find(DynamicForm::class.java, this.formId!!)

			if (this.pageTemplate != null && this.pageTemplate!!.id != null && this.pageTemplate!!.id > 0)
			{
				if (form != null)
				{
					form.pageTemplate = this.pageTemplate
				}
			}
			else
			{
				form!!.pageTemplate = null
			}

			this.daoUtil.update(form!!)
		}

		GeneralUtil.addMessage("Template settings have been saved.", FacesMessage.SEVERITY_INFO)
	}
}
