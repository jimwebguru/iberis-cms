package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.ocpsoft.pretty.PrettyContext

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList
import java.util.Hashtable


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class CmsUtil : Serializable
{
	private val daoUtil = DaoUtil()

	var regionPageAssignments: Hashtable<String, List<PageItemAssignment>> = Hashtable()

	var pageRegions: Collection<PageRegion>? = null


	@PostConstruct
	fun loadPageAssignments()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val pageRegions: Collection<PageRegion>?

		if (PageUtil.pageObjectExists())
		{
			val pageLayout = PageUtil.pageObjectLayout

			if (pageLayout != null)
			{
				pageRegions = pageLayout.pageRegions
			}
			else
			{
				pageRegions = getPageRegionsByContext(cmsSite)
			}
		}
		else
		{
			pageRegions = getPageRegionsByContext(cmsSite)
		}

		this.pageRegions = pageRegions

		val scope = PageUtil.pageObjectScope
		val onlyScope = PageUtil.allowOnlyScope

		if (pageRegions != null && pageRegions.isNotEmpty())
		{
			val request = GeneralUtil.request

			var debugMode = false

			if (request!!.getParameter("layout-debug") != null && request.getParameter("layout-debug") == "1")
			{
				debugMode = true
			}

			for (region in pageRegions)
			{
				var path = PrettyContext.getCurrentInstance().requestURL.toString()

				if (path == "")
				{
					path = "/"
				}

				if (path != "/")
				{
					val lastCharacter = path.substring(path.length - 1)

					if (lastCharacter != "/")
					{
						path += "/"
					}
				}

				val queryParameters = ArrayList<QueryParameter>()

				var queryParameter = QueryParameter("regionId", region.id)
				queryParameters.add(queryParameter)

				queryParameter = QueryParameter("siteId", cmsSite.siteId!!)
				queryParameters.add(queryParameter)

				var pageItemQuery = "SELECT OBJECT(o) FROM PageItemAssignment o WHERE o.pageRegion.id = :regionId AND o.site.siteID = :siteId"

				if(!debugMode && onlyScope && scope != null && scope.isNotEmpty())
				{
					pageItemQuery += " AND o.scope = :scope"
					queryParameter = QueryParameter("scope", scope)
					queryParameters.add(queryParameter)
				}

				val assignments = this.daoUtil.find<PageItemAssignment>("$pageItemQuery ORDER BY o.sequence", queryParameters)
				val allowedAssignments = ArrayList<PageItemAssignment>()

				for (assignment in assignments!!)
				{
					var addAssignment = true

					if (assignment.allowedUrls != null && assignment.allowedUrls.isNotEmpty())
					{
						var addTemp = false

						for (allowedUrl in assignment.allowedUrls)
						{
							var url = allowedUrl.url

							if (!url.startsWith("/") && path.startsWith("/"))
							{
								url = "/" + url
							}

							val lastCharacter = url.substring(url.length - 1)

							if (lastCharacter != "/" && lastCharacter != "*")
							{
								url += "/"
							}

							if (lastCharacter == "*")
							{
								if (path.startsWith(url.replace("*", "")))
								{
									addTemp = true
								}
							}
							else
							{
								if (url == path)
								{
									addTemp = true
								}
							}
						}

						addAssignment = addTemp
					}

					if (assignment.deniedUrls != null && assignment.deniedUrls.isNotEmpty())
					{
						for (deniedUrl in assignment.deniedUrls)
						{
							var url = deniedUrl.url

							if (!url.startsWith("/") && path.startsWith("/"))
							{
								url = "/" + url
							}

							val lastCharacter = url.substring(url.length - 1)

							if (lastCharacter != "/" && lastCharacter != "*")
							{
								url += "/"
							}

							if (lastCharacter == "*")
							{
								if (path.startsWith(url.replace("*", "")))
								{
									addAssignment = false
								}
							}
							else
							{
								if (url == path)
								{
									addAssignment = false
								}
							}
						}
					}

					assignment.inScope = true

					if(addAssignment)
					{
						if(debugMode && onlyScope && scope != null && scope.isNotEmpty())
						{
							if(assignment.scope == null || assignment.scope != scope)
							{
								addAssignment = false
								assignment.inScope = false
							}
						}

						if(assignment.scope != null && assignment.scope.isNotEmpty() && (scope == null || scope != assignment.scope))
						{
							addAssignment = false
							assignment.inScope = false
						}
					}

					if (!debugMode)
					{
						if (addAssignment)
						{
							allowedAssignments.add(assignment)
						}
					}
					else
					{
						assignment.pageRendered = addAssignment
						allowedAssignments.add(assignment)
					}
				}

				if (allowedAssignments.size > 0)
				{
					this.regionPageAssignments.put(region.id!!.toString(), allowedAssignments)
				}
			}
		}
	}

	private fun getPageRegionsByContext(cmsSite: CmsSite): Collection<PageRegion>
	{
		return if (PrettyContext.getCurrentInstance().requestURL.toString() == "/" || PrettyContext.getCurrentInstance().requestURL.toString().isEmpty())
		{
			this.daoUtil.find(Site::class.java, cmsSite.site!!.siteID)!!.frontPageTemplate.pageRegions
		}
		else
		{
			this.daoUtil.find(Site::class.java, cmsSite.site!!.siteID)!!.innerPageTemplate.pageRegions
		}
	}

	fun getPageRegionTitle(regionId: Long): String
	{
		val pageRegion = this.daoUtil.find(PageRegion::class.java, regionId)

		return pageRegion!!.name
	}

	fun getContent(id: Long): Content?
	{
		return this.daoUtil.find(Content::class.java, id)
	}

	fun getMenu(id: Long): Menu?
	{
		return this.daoUtil.find(Menu::class.java, id)
	}

	fun getGallery(id: Long): Gallery?
	{
		return this.daoUtil.find(Gallery::class.java, id)
	}

	fun getPresentation(id: Long): Presentation?
	{
		return this.daoUtil.find(Presentation::class.java, id)
	}

	fun getImage(id: Long): Image?
	{
		return this.daoUtil.find(Image::class.java, id)
	}

	fun getDynamicForm(id: Long): DynamicForm?
	{
		return this.daoUtil.find(DynamicForm::class.java, id)
	}

	fun getProductCatalog(id: Long): ProductCatalog?
	{
		return this.daoUtil.find(ProductCatalog::class.java, id)
	}

	fun getVideo(id: Long): Video?
	{
		return this.daoUtil.find(Video::class.java, id)
	}

	fun getBlog(id: Long): Blog?
	{
		return this.daoUtil.find(Blog::class.java, id)
	}

	fun getRecentBlogEntries(blogId: Long) : List<BlogEntry>?
	{
		return this.daoUtil.find("SELECT OBJECT(o) FROM BlogEntry o WHERE o.blog.id = $blogId ORDER BY o.publishedDate DESC", 15)
	}
}
