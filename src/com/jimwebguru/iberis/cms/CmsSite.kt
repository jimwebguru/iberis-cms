package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil

import javax.annotation.PostConstruct

import javax.enterprise.context.ApplicationScoped
import javax.inject.Named

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ApplicationScoped
class CmsSite
{
	var siteId: Long? = null

	var site: Site? = null

	private val daoUtil = DaoUtil()

	var siteUrl: String? = null

	@PostConstruct
	fun postInit()
	{
		val cmsSiteId = GeneralUtil.getWebXmlEnvParam("cmsSiteId")

		if (cmsSiteId != null)
		{
			this.siteId = cmsSiteId as Long

			this.site = this.daoUtil.find(Site::class.java, this.siteId!!)

			this.siteUrl = this.site?.url
		}
		else
		{
			val serverUrl = GeneralUtil.request!!.scheme + "://" + GeneralUtil.request!!.serverName + ":" + (if (GeneralUtil.request!!.serverPort != 80) GeneralUtil.request!!.serverPort else "") + GeneralUtil.request!!.contextPath

			this.siteUrl = serverUrl
		}
	}

	fun reloadSite()
	{
		this.site = this.daoUtil.find(Site::class.java, this.siteId!!)

		if(this.site!!.url != null && this.site!!.url.isNotEmpty())
		{
			this.siteUrl = this.site!!.url
		}
	}

}
