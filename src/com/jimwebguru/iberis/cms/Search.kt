package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import com.jimwebguru.iberis.core.persistence.*

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class Search : Serializable
{
	var contentList: List<Content>? = null
	var imageList: List<Image>? = null
	var galleryList: List<Gallery>? = null
	var presentationList: List<Presentation>? = null
	var videoList: List<Video>? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun postInit()
	{
		val cmsSite = GeneralUtil.getManagedBean("cmsSite") as CmsSite

		val searchCriteria = GeneralUtil.request!!.getParameter("search-criteria")

		var searchValue = ""

		if (searchCriteria != null)
		{
			searchValue = searchCriteria.toString().toLowerCase()
		}

		this.contentList = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Content o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} AND (LOWER(o.title) LIKE '%$searchValue%' OR LOWER(o.body) LIKE '%$searchValue%' OR LOWER(o.body2) LIKE '%$searchValue%' OR LOWER(o.body3) LIKE '%$searchValue%' OR LOWER(o.body4) LIKE '%$searchValue%') ORDER BY o.id DESC")
		this.imageList = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Image o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} AND LOWER(o.name) LIKE '%$searchValue%' OR LOWER(o.filePath) LIKE '%$searchValue%' ORDER BY o.imageid DESC")
		this.galleryList = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Gallery o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} AND LOWER(o.name) LIKE '%$searchValue%' ORDER BY o.id DESC")
		this.presentationList = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Presentation o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} AND LOWER(o.name) LIKE '%$searchValue%' ORDER BY o.id DESC")
		this.videoList = this.daoUtil.findRange(intArrayOf(0, 50), "SELECT DISTINCT OBJECT(o) FROM Video o JOIN o.sites s WHERE s.siteID = ${cmsSite.siteId} AND LOWER(o.name) LIKE '%$searchValue%' OR LOWER(o.filePath) LIKE '%$searchValue%' ORDER BY o.id DESC")

	}
}
