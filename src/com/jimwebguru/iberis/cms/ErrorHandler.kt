package com.jimwebguru.iberis.cms

import javax.enterprise.context.RequestScoped
import javax.faces.context.FacesContext
import javax.inject.Named

import java.io.PrintWriter
import java.io.StringWriter


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@RequestScoped
class ErrorHandler
{
	val statusCode: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.status_code"]?.toString()

	val message: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.message"]?.toString()

	val exceptionType: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.exception_type"]?.toString()

	val exception: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.exception"]?.toString()

	val requestURI: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.request_uri"]?.toString()

	val servletName: String?
		get() = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.servlet_name"]?.toString()

	val stackTrace: String?
		get()
		{
			val sw = StringWriter()
			val pw = PrintWriter(sw)

			val errorObject = FacesContext.getCurrentInstance().externalContext.requestMap["javax.servlet.error.exception"]

			if(errorObject is StackOverflowError)
			{
				errorObject.printStackTrace(pw)
			}
			else
			{
				(errorObject as Exception).printStackTrace(pw)
			}

			return sw.toString()
		}
}
