package com.jimwebguru.iberis.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import java.io.Serializable
import java.util.ArrayList
import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class BlogView : Serializable
{
	private val daoUtil = DaoUtil()
	
	var blogEntryLoader = GenericLazyLoader(BlogEntry::class.java)

	var blog: Blog? = null
	var blogEntry : BlogEntry? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val blogUrlAlias = request!!.getParameter("blogUrlAlias")

		if(blogUrlAlias != null && blogUrlAlias.isNotEmpty())
		{
			val blogs = this.daoUtil.find<Blog>("SELECT OBJECT(o) FROM Blog o WHERE o.urlAlias = '$blogUrlAlias'")
			blog = blogs!![0]

			val queryParameters = ArrayList<QueryParameter>()

			val queryParameter = QueryParameter("blogId", blog!!.id)
			queryParameter.explicitCondition = "o.blog.id = :blogId"

			queryParameters.add(queryParameter)

			this.blogEntryLoader.additionalQueryParams = queryParameters
		}

		val entryUrlAlias = request.getParameter("blogEntryUrlAlias")

		if(entryUrlAlias != null && entryUrlAlias.isNotEmpty())
		{
			val blogEntries = this.daoUtil.find<BlogEntry>("SELECT OBJECT(o) FROM BlogEntry o WHERE o.blog.id = ${blog!!.id} AND o.urlAlias = '$entryUrlAlias'")

			this.blogEntry = blogEntries!![0]
		}
	}
}